-- PRAGMA foreign_keys = ON;
CREATE TABLE IF NOT EXISTS `Customers` (
`Customer_ID` INTEGER NOT NULL,
`Customer_Details` VARCHAR(255) NOT NULL,
PRIMARY KEY (Customer_ID)
);
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (1, 'America Jaskolski');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (2, 'Ellsworth Paucek');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (3, 'Mrs. Hanna Willms');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (4, 'Dr. Diana Rath');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (5, 'Selena Gerhold');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (6, 'Lauriane Ferry PhD');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (7, 'Sydnie Friesen');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (8, 'Dayana Robel');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (9, 'Mr. Edwardo Blanda I');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (10, 'Augustine Kerluke');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (11, 'Buddy Marquardt');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (12, 'Mr. Randal Lynch III');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (13, 'Mrs. Liza Heller V');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (14, 'Mrs. Lilly Graham III');
INSERT INTO `Customers` (`Customer_ID`, `Customer_Details`) VALUES (15, 'Miss Felicita Reichel');

CREATE TABLE IF NOT EXISTS `Customer_Policies` (
    `Policy_ID` INTEGER NOT NULL,
    `Customer_ID` INTEGER NOT NULL,
    `Policy_Type_Code` CHAR(15) NOT NULL,
    `Start_Date` DATE,
    `End_Date` DATE,
PRIMARY KEY (Policy_ID),
FOREIGN KEY (Customer_ID) REFERENCES Customers (Customer_ID)
);

INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (119, 1, 'Car', '2018-01-21', '2017-12-15');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (141, 2, 'Life', '2017-08-21', '2017-09-29');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (143, 3, 'Car', '2017-06-16', '2017-12-09');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (218, 4, 'Car', '2017-09-18', '2017-11-23');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (264, 4, 'Car', '2016-12-25', '2018-01-25');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (270, 5, 'Life', '2016-07-17', '2018-01-05');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (352, 6, 'Property', '2016-05-23', '2017-12-09');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (396, 7, 'Travel', '2017-07-30', '2017-10-09');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (473, 3, 'Travel', '2017-04-24', '2017-12-14');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (552, 12, 'Travel', '2017-12-13', '2017-11-05');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (587, 13, 'Travel', '2017-03-23', '2017-09-01');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (738, 8, 'Travel', '2018-06-16', '2017-12-04');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (744, 6, 'Property', '2017-12-01', '2018-03-07');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (858, 9, 'Property', '2016-05-30', '2018-02-11');
INSERT INTO `Customer_Policies` (`Policy_ID`, `Customer_ID`, `Policy_Type_Code`, `Start_Date`, `End_Date`) VALUES (900, 2, 'Property', '2017-01-20', '2017-12-11');

CREATE TABLE IF NOT EXISTS `Claims` (
`Claim_ID` INTEGER NOT NULL,
`Policy_ID` INTEGER NOT NULL,
`Date_Claim_Made` DATE,
`Date_Claim_Settled` DATE,
`Amount_Claimed` INTEGER,
`Amount_Settled` INTEGER,
PRIMARY KEY (Claim_ID),
FOREIGN KEY (Policy_ID) REFERENCES Customer_Policies (Policy_ID)
);

INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (143, 744, '2017-03-11', '2017-11-03', 43884, 1085);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (423, 552, '2016-08-12', '2018-01-27', 79134, 1724);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (442, 473, '2017-02-24', '2018-01-21', 70088, 1189);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (486, 141, '2018-06-14', '2017-12-20', 69696, 1638);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (546, 744, '2017-05-03', '2017-12-22', 46479, 1091);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (563, 141, '2016-08-02', '2017-09-04', 41078, 1570);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (569, 473, '2018-07-15', '2017-11-19', 49743, 930);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (571, 858, '2017-08-03', '2018-02-18', 89632, 1528);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (621, 744, '2016-12-18', '2018-01-11', 43708, 1652);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (761, 473, '2016-08-26', '2017-09-04', 83703, 1372);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (801, 738, '2017-10-21', '2018-01-05', 3326, 1353);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (843, 143, '2017-10-14', '2018-02-20', 10209, 1639);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (935, 143, '2018-07-13', '2017-11-22', 70674, 1637);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (957, 352, '2018-11-08', '2017-09-15', 38280, 1050);
INSERT INTO `Claims` (`Claim_ID`, `Policy_ID`, `Date_Claim_Made`, `Date_Claim_Settled`, `Amount_Claimed`, `Amount_Settled`) VALUES (965, 119, '2017-07-17', '2018-03-09', 35824, 1636);


